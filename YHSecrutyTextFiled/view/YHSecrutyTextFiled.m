//
//  SecrutyTextFiled.m
//  NewKroreaCards
//
//  Created by victorLiu on 2017/5/23.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import "YHSecrutyTextFiled.h"

@interface YHSecrutyTextFiled ()<UITextFieldDelegate>
@property(nonatomic,strong)NSMutableArray *labelArrary;
@property(nonatomic,assign)NSInteger flag;
@property(nonatomic,strong)NSMutableString *value;

@end

@implementation YHSecrutyTextFiled
-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        _flag = 0;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 0.5f;
        self.layer.cornerRadius = 4;
        
        UITextField *textfield = [[UITextField alloc]initWithFrame:self.bounds];
        [textfield setKeyboardType:UIKeyboardTypeNumberPad];
        [self addSubview:textfield];
        [textfield becomeFirstResponder];
        textfield.hidden = YES;
        textfield.delegate =self;
        
        for (NSInteger i = 0; i <6; i++) {
            UIImageView *label = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/6*i, 0, frame.size.width/6, frame.size.height)];
            CALayer *layer =[[CALayer alloc]init];
            if (i < 5) {
                [layer setFrame:CGRectMake(label.frame.size.width -0.5f, 0, 0.5f, label.frame.size.height)];
            }
            layer.backgroundColor = [UIColor lightGrayColor].CGColor;
            [label.layer addSublayer:layer];
            [self.labelArrary addObject:label];
            [self addSubview:label];
            
            
        }
    }
    return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length > 0) {
        if (_flag < 6) {
            _flag ++;
            self.value =  [[self.value stringByAppendingString:string] mutableCopy];
        }
    }else{
        _flag --;
        [self.value replaceCharactersInRange:NSMakeRange(_value.length -1, 1) withString:@""];
    }
    
    for (NSInteger i = 0; i <_labelArrary.count; i++) {
        UIImageView *label = _labelArrary[i];
        if (i < _flag) {
            label.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"圆点" ofType:@"png"]];
        }else{
            label.image =nil;
        }
    }
    if (_flag == 6) {
        if (self.valueConfrim != nil) {
           self.valueConfrim(_value);  
        }
       
    }
//    NSLog(@"string = %@ --%ld",self.value,(long)_flag);
    if (_flag < 7) {
        return YES;
    }
    _flag = 6;
    return NO;
}

-(NSMutableArray *)labelArrary{
    if (!_labelArrary) {
        _labelArrary = [[NSMutableArray alloc]init];
    }
    return _labelArrary;
}

-(NSMutableString *)value{
    if (!_value) {
        _value = [[NSMutableString alloc]init];
    }
    return _value;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
